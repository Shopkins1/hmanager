import java.util.ArrayList;


public class Dungeon {
	public static void adventure(int DungeonDifficulty, int startlevel, int numberoflevelsexplored, ArrayList<Class> Party){
		int currentlevel = startlevel; //level iteration
		int damage = 0; //monster damage initialisation
		int monsterhealth; //monster health initialisation
		int damagetaken = 0; //damage taken initialisation
		int iterat = 0; //random iterator initialisation
		ArrayList<String> Deathlevel = new ArrayList<String>(); //list of where the deaths occurred
		ArrayList<Class> Deathee = new ArrayList<Class>(); //list of who dies


		//~~~~~~~~~~DUNGEON START~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		while(currentlevel != (startlevel + numberoflevelsexplored)){ //go through all levels intended
			damage = (int)(Math.random() * (currentlevel + (DungeonDifficulty*2))); //randomly generate damage from monster based on level
			if (damage == 0){
				//System.out.print("No monster's found on this level\n"); //if damage generated as 0 then no monsters
			}
			else{

				//~~~~~~~~~BATTLE START~~~~~~~~~~~~~
				monsterhealth = damage + currentlevel + DungeonDifficulty;
				//If ranged, deal damage before
				for (Class s : Party) {
					if(s.Type == 1){
						monsterhealth -= s.attack;
					}
				}
				while(monsterhealth > 0){
					double sdamage = damage; //So I party can take less damage further away from front
					for (Class s : Party) {
						//iterate through and do damage (sort of split but not really)
						if(s.defense < sdamage){
							s.altercurrenthealth(-((int)sdamage - s.defense));
							damagetaken += (int)sdamage - s.defense; //damage total for reasons
						}
						sdamage = Math.ceil((sdamage*0.9));
					}

					for (Class s : Party) {
						//check if anyone dead
						if (s.currenthealth < 1){
							Deathee.add(s);
							Deathlevel.add(String.valueOf(currentlevel));
						}

					}

					//remove the deaduns from party and from adventurers (CAUSE DEY DEADED)
					for (Class removes : Deathee){
						Party.remove(removes);
						Game.Adventurers.remove(removes);
						Game.OutandAbout.remove(removes);
					}

					//Then Check if everyone dead
					if(Party.size() == 0){
						System.out.print("The entire party has fallen \n");

						iterat = 0; //Cry over deaths
						for (Class t : Deathee) {
							System.out.print(t.name + " fought bravely but was felled by a mighty foe on floor " + Deathlevel.get(iterat) + ". Goodnight " + t.name + "\n");
							iterat ++;
						}
						return;						
					}


					for (Class s : Party) {
						//party damages beast
						monsterhealth -= s.attack;
					}
				}
				//~~~~~~~~~~~~~~~~~~~~~BATTLE END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

				for (Class s : Party) {
					//Experience Get
					s.gainexperience(currentlevel*10);
				}

			}
			/*//TESTING PURPOSES
			System.out.print("##### \n");
			for (Class s : Party) {
				System.out.print(s.name + "'s health is now " + s.currenthealth + " Exp at " + s.experience + "\n");
			    }
			System.out.print("##### \n");
			//END TESTING PURPOSES*/
			currentlevel ++;//go up(or down however you want this crazy dungeon to work) a level
		}	//
		//~~~~~~~~~~~~~~~~~~~~~~DUNGEON END~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




		//Congratulatory message
		System.out.print(Party.get(0).name + "'s Party returned with a horde of loot, good job " + Party.get(0).name +  ", " + Party.get(0).name + "'s party took " + damagetaken + " Damage, yay \n" );	
		//talk about level ups and remove party members from the out and about list
		for(Class t : Party){
			if(t.levelgets > 0){
				System.out.print(t.name + " Gained " + t.levelgets + " level(s), WOW! \n");
				System.out.print(t.name + " Now has " + t.currenthealth + " Health remaining, WOW! \n");
				t.levelgets = 0;
			}
			Game.OutandAbout.remove(t);
		}

		//Cry over deaths
		if(Deathee.size() > 0){
			System.out.print("Unfortunately, the following party members have perished: \n");
			iterat = 0; 
			for (Class t : Deathee) {
				System.out.print(t.name + ", who fought bravely, but was ultimately killed by a mighty foe on floor " + Deathlevel.get(iterat) + ". Goodnight " + t.name + "\n");
				iterat ++;
			}
		}
	}
}