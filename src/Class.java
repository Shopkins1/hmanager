
public class Class {
	public int attack;
	public int maxhealth;
	public int Type; //0 = warrior 1=ranger
	public int currenthealth;
	public int defense;
	public int experience;
	public int level;
	public int speed;
	public String name;
	public int levelgets;
	public Class (String aname, int starttype, int startattack, int starthealth, int startdefense, int startlevel){
		attack = startattack;
		maxhealth = starthealth;
		currenthealth = starthealth;
		defense = startdefense;
		Type = starttype;
		experience = 0;
		level = startlevel;
		levelgets = 0;
		name = aname;
		Game.Adventurers.add(this);
	}
	public void altercurrenthealth(int change){
		currenthealth += change;
	}
	public void alterspeed(int change){
		speed += change;
	}
	public void altermaxhealth(int change){
		maxhealth += change;
		currenthealth += change ;
	}
	public void fullhealth(){
		currenthealth = maxhealth;
	}
	public void alterattack(int change){
		attack += change;
	}
	public void alterdefense(int change){
		defense += change;
	}
	public void gainexperience(int change){
		experience += change;
		while (experience > (100 * this.level)){
			this.alterattack(1);
			this.alterdefense(1);
			this.altermaxhealth(1);
			this.alterspeed(1);
			experience -= (100 * this.level);
			level += 1;
			levelgets += 1;
			;
		}
		
	}
}
