import java.util.ArrayList;
import java.util.Scanner;

public class Game {
private static Scanner user_input = new Scanner( System.in );
public static ArrayList<Class> Adventurers = new ArrayList<Class>();
public static ArrayList<Class> OutandAbout = new ArrayList<Class>();
	
	public static void main(String[] args){
		Class brian = new Class("Brian", 0, 5, 20, 1, 1);
		Class brian2 = new Class("Brian2", 1, 5, 10, 1, 1);
		while(true){
			StartAdventure();
			//Much better
		}
	}

	public static void listadventurers(ArrayList<Class> Adv){ //read out a lst
		System.out.print("Your current Adventurers include: \n");
		for(Class Adventurer : Adv){			
			if(Adventurer.Type == 0){
				System.out.print(Adventurer.name + " the Warrior \n");
			}
			else{
				System.out.print(Adventurer.name + " the Archer \n");
			}
		}
	}

	
	
	
	
	
private static void StartAdventure(){
	listadventurers(Adventurers);
	int finish = 0; //chose marker
	ArrayList<Class> Party = new ArrayList<Class>(); //makes a party list
	
	//asks for first Party Member (CAN EXIT AT ANY TIME BY TYPING Exit)
	System.out.print("Which would you like to be the Party Leader? (#case sensitive) (Exit = exits) \n");
	finish = tolst(Party);
	if(finish == 2){
		System.out.print("Nevermind then \n");
		return;
	}
	
	
	//asks for party members, while loops are so no wrong chooses can be made
	while(finish != 1000){
		System.out.print("Any other Party members? (No to finish) \n");
		finish = tolst(Party);
		if(finish == 2){
			System.out.print("Nevermind then \n");
			return;
		}
		if(finish == 1){
			while(finish != 1000){
				System.out.print("Is this party Correct? (No to start again) \n");
				listadventurers(Party);
				finish = tolst(Party);
				if(finish == 2){
					System.out.print("Nevermind then \n");
					return;
				}
				if(finish == 1){
					System.out.print("OK... let's try again \n");
					StartAdventure();
					return;
				}
				if(finish == 3){
					System.out.print("Then Let's Go! \n");
					for(Class t : Party){
						OutandAbout.add(t);
					}
					Dungeon.adventure(1, 1, 5, Party); //change other variables at some point
					return;
				}
			}

		}
	}
}
private static int tolst( ArrayList<Class> lst){ //get responses
	String pick = user_input.next();
	for(Class Adventurer : Adventurers){
		if(pick.equals("Exit")){
			return 2;
		}
		if(pick.equals("No")){
			return 1;
		}
		if(pick.equals("Yes")){
			return 3;
		}
		if(pick.equals(Adventurer.name)){
			if(OutandAbout.contains(Adventurer)){
				System.out.print(Adventurer.name + " is already on a mission \n");
				return 0;
			}
			else{
				if(lst.contains(Adventurer)){
					System.out.print(Adventurer.name + " is already in this party \n");
					return 0;
				}
				else{
					lst.add(Adventurer);
					System.out.print("Ok, We'll put " + Adventurer.name + " in this party \n");
					return 0;
				}
			}	
		}
	}
	System.out.print("Could not find, please try again \n");
	int blah = tolst(lst);
	return blah;
}

}